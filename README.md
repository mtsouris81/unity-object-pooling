# Object Pool for Unity

### Problem  
Whether you�re spawning NPC's or throwing stars, new allocations during runtime hurt performance. New allocations keep the Garbage Collector active, and lead to unpredictable memory usage.


### Solution
Pre-allocate a pool of objects that will stay in existence during runtime. The objects will be resettable and the pool will make them available in order. This avoids new allocations and unnecessary garbage collection. This allows you to predict how much memory and resources are associated with a pool of objects.



## Getting Started

- Clone or download the repo
- Use Unity3D Editor to open the project.
- Run one of the demo scenes to see an example in use. demo_comparison  or  demo_simple


### Prerequisites
- Visual Studio 2017 Free Community Version
- Unity3D 2018 Free Version


## Using in your own projects

Add the ObjectPooling folder into your project. You can delete \_demo\_only and scenes if you already know how to use this system.

### Prepare Items for the Pool

The items you wish to use in the object pool must have a component attached at the root level that implements the AbstractPooledItem class. This class enforces a workflow for sending data to each instance and activating on demand. There are some pre-existing implementations of this class: GenericPooledItem and  RigidBodyPooledItem. 

As you decide the types of items that will need to be pooled, go to the code file:

- ObjectPooling\scripts\core\FromThePool.cs 

Edit the list to have whatever items you want. This is the enum you will use to specify what type of item you want in code. 

### Making the Pool available in the scene
- Let Unity Editor rebuild the code after you have edited that code file (usually happens automatically). 
- In the top menu strip of Unity Editor, click  GameObject > Object Pool > Generate Containers
- A GameObject named POOL will be added to the scene hierarchy. This POOL object contains child objects for each of the items listed in FromThePool.cs
- Now drag prefab instances into the containers you wish to use for calling those instances from the Pool.
- In code, you can use the code below to get your instances.


    var impactInstance = ObjectPool.Current.GetNext(FromThePool.ImpactExplosion);
    impactInstance.SetData(v);
    impactInstance.Activate();



### Implementing your own classes for AbstractPooledItem 

public override void SetData(params object[] args)

The goal of this method is to 'reset' the object and make it ready for 'fresh use'. In the case of a projectile, you would want to set the object's position and velocity. So an implementation would look like this:


        public override void SetData(params object[] args)
        {
		
            // assuming a member called body of type RigidBody exists at the class level


            // clear out rigid body forces
            body.angularVelocity = Vector3.zero;
            body.velocity = Vector3.zero;
            body.rotation = Quaternion.identity;

            //position
            if (args.Length > 0)
            {
                this.transform.position = (Vector3)args[0];
            }
            // velocity
            if (args.Length > 1)
            {
                body.velocity = (Vector3)args[1];
            }
        }



Lots of different Pooled items will have different data requirements, and that's why the pattern of using an object array was used. Look at GenericPooledItem and  RigidBodyPooledItem classes for examples of how this abstract class can be implemented. 




## Built With
- Visual Studio 2017 Free Community Version
- Unity3D 2018 Free Version
- C#




