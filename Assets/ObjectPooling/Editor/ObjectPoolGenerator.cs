﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Optimization;

public class ObjectPoolGenerator : EditorWindow
{

    [MenuItem("GameObject/Object Pool/Generate Containers")]
    static void Init()
    {
        ObjectPool pool = GetNewOrExistingObjectPool();
        List<FromThePool> neededTypes = GetNeededPoolContainers(pool);
        foreach (var poolType in neededTypes)
        {
            CreatePoolSet(pool, poolType);
        }
    }

    static void CreatePoolSet(ObjectPool parentPool, FromThePool t)
    {
        ObjectPoolSet poolSet = new GameObject(t.ToString(), typeof(ObjectPoolSet)).GetComponent<ObjectPoolSet>();
        poolSet.transform.SetParent(parentPool.transform);
        poolSet.PoolType = t;
    }

    static ObjectPool GetNewOrExistingObjectPool()
    {
        var existingPool = FindObjectOfType<ObjectPool>();
        if (existingPool != null)
        {
            return existingPool;
        }
        return new GameObject("POOL", typeof(ObjectPool)).GetComponent<ObjectPool>();
    }
    
    static List<FromThePool> GetNeededPoolContainers(ObjectPool pool)
    {
        var result = EnumGetAll<FromThePool>();
        if (pool == null)
        {
            return result;
        }
        var existingSets = pool.GetComponentsInChildren<ObjectPoolSet>();
        if (existingSets.Length > 0)
        {
            for (int i = 0; i < result.Count; i++)
            {
                var targetType = result[i];
                if (existingSets.Any(s => s.PoolType == targetType))
                {
                    // there is already an existing PoolSet of this type, remove it from the 'needed' list
                    result.Remove(result[i]);
                    i--;
                }
            }
        }
        return result;
    }

    static List<T> EnumGetAll<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>().ToList();
    }
}