﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleActivator : MonoBehaviour
{
    ParticleSystem _particles;
    ParticleSystem.EmissionModule _emission;

    private void EnsureElements()
    {
        if (_particles == null)
        {
            _particles = GetComponent<ParticleSystem>();
            _emission = _particles.emission;
        }
    }

    private void OnEnable()
    {
        EnsureElements();
        _particles.Play();
    }

}
