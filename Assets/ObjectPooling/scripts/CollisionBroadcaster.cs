﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBroadcaster : MonoBehaviour
{
    public BasicCollisionAction OnCollision = BasicCollisionAction.DoNothing;

    public Action<Vector3> OnCollisionEvent { get; set; }

    private void OnCollisionEnter(Collision collision)
    {
        if (OnCollisionEvent != null)
        {
            OnCollisionEvent(collision.contacts[0].point);
        }
        if (OnCollision == BasicCollisionAction.Disable)
        {
            this.gameObject.SetActive(false);
        }
        if (OnCollision == BasicCollisionAction.Destory)
        {
            Destroy(this.gameObject, 0.05f);
        }
    }

    public enum BasicCollisionAction
    {
        DoNothing,
        Disable,
        Destory
    }

}
