﻿
using System;

namespace UnityEngine.Optimization
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidBodyPooledItem : AbstractPooledItem
    {
        Rigidbody body;

        private void EnsureElements()
        {
            if (body == null)   
                body = GetComponent<Rigidbody>();
        }
        

        /// <summary>
        /// Sets data for a Rigid Body in motion with optional callback
        /// </summary>
        /// <param name="args">POSITION  VELOCITY   COLLISION-CALLBACK(VECTOR3)</param>
        public override void SetData(params object[] args)
        {
            EnsureElements();
            // clear out rigid body forces
            body.angularVelocity = Vector3.zero;
            body.velocity = Vector3.zero;
            body.rotation = Quaternion.identity;


            //position
            if (args.Length > 0)
            {
                this.transform.position = (Vector3)args[0];
            }
            // velocity
            if (args.Length > 1)
            {
                body.velocity = (Vector3)args[1];
            }


            // callback (optional)
            var broadcaster = GetComponent<CollisionBroadcaster>();
            if (broadcaster != null && args.Length > 2)
            {
                var action = args[2] as Action<Vector3>;
                if (action != null)
                {
                    broadcaster.OnCollisionEvent = action;
                }
            }
        }
    }
}