﻿using System.Collections.Generic;

namespace UnityEngine.Optimization
{
    public class ObjectPool : MonoBehaviour
    {
        public static ObjectPool Current { get; private set; }
        Dictionary<FromThePool, ObjectPoolSet> pools;

        public void Awake()
        {
            if (Current != null)
            {
                Debug.LogError("Only one instance of GameSpecificObjectPool should exist");
                return; 
            }
            Current = this; 
            ObjectPoolSet[] list = GetComponentsInChildren<ObjectPoolSet>(true);
            pools = new Dictionary<FromThePool, ObjectPoolSet>();
            foreach (var i in list)
            {
                if (pools.ContainsKey(i.PoolType) == false)
                    pools.Add(i.PoolType, null);

                pools[i.PoolType] = i;
            }
        }

        public AbstractPooledItem GetNext(FromThePool want)
        {
            if (pools == null)
                throw new System.Exception("this pbject pool has not been properly setup. Cannot get next item."); 

            return pools[want].GetNext();
        }
    }
}