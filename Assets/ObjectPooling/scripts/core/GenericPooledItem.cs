﻿
namespace UnityEngine.Optimization
{
    public class GenericPooledItem : AbstractPooledItem
    {
        public override void SetData(params object[] args)
        {
            //position
            if (args.Length > 0)
            {
                this.transform.position = (Vector3)args[0];
            }
        }
    }
}