﻿using System.Collections.Generic;

namespace UnityEngine.Optimization
{
    public class ObjectPoolSet : MonoBehaviour
    {
        public FromThePool PoolType;

        private Queue<AbstractPooledItem> queue;

        private void Awake()
        {
            int count = this.transform.childCount;
            queue = new Queue<AbstractPooledItem>(count);
            for (int i = 0; i < count; i++)
            {
                var element = GetPooledItemAtIndexOf(i);
                if (element != null)
                {
                    queue.Enqueue(element);
                }
            }
        }
        private AbstractPooledItem GetPooledItemAtIndexOf(int index)
        {
            return this.transform.GetChild(index).GetComponent<AbstractPooledItem>();
        }
        public AbstractPooledItem GetNext()
        {
            var result = queue.Dequeue();
            queue.Enqueue(result);
            return result;
        }
    }
}