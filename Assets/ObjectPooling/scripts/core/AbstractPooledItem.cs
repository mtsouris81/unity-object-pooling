﻿namespace UnityEngine.Optimization
{
    public abstract class AbstractPooledItem : MonoBehaviour
    {
        public abstract void SetData(params object[] args);
        public void Activate()
        {
            this.gameObject.SetActive(false);
            this.gameObject.SetActive(true);
        }
    }
}
