﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFadeDown : MonoBehaviour {

    public float FadeTime;
    public float MaxIntensity = 2;
    public float MinIntensity = 0;

    float _time;
    Light _light;

    private void EnsureElements()
    {
        if (_light == null)
        {
            _light = GetComponent<Light>();
        }
    }

    private void OnEnable()
    {
        EnsureElements();
        _time = FadeTime;
    }

    private void Update ()
    {
        if (_time > 0)
        {
            _time -= Time.deltaTime;
            if (_time <= 0)
            {
                _light.range = 0;
                return;
            }
            else
            {
                _light.range = Mathf.Lerp(MinIntensity, MaxIntensity , (_time / FadeTime));
            }
        }
	}
}
