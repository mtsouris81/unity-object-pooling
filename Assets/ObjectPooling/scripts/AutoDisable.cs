﻿
namespace UnityEngine.Optimization
{
    public class AutoDisable : MonoBehaviour
    {
        public float ExpireTime;
        public bool UnscaledDeltaTime = false;
        public bool ShouldDestroy = false;

        float _time;

        private void OnEnable()
        {
            _time = ExpireTime;
        }

        private void Update()
        {
            if (_time > 0)
            {
                if (UnscaledDeltaTime)
                {
                    _time -= Time.unscaledDeltaTime;
                }
                else
                {
                    _time -= Time.deltaTime;
                }

                if (_time <= 0)
                {
                    if (ShouldDestroy)
                    {
                        Destroy(this.gameObject, 0.01f);
                    }
                    else
                    {
                        // 'expire'
                        this.gameObject.SetActive(false);
                    }
                }
            }
        }
    }
}
