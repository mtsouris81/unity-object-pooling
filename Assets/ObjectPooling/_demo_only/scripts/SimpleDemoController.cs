﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Optimization;

public class SimpleDemoController : MonoBehaviour
{

    public Camera FocusCamera;
    public FromThePool BulletItem;
    public FromThePool ImpactItem;
    public float ProjectileSpeed = 6;


	void Update ()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            SpawnBullet();
        }
    }


    public void SpawnBullet()
    {
        var ray = FocusCamera.ScreenPointToRay(Input.mousePosition);
        var createdItem = ObjectPool.Current.GetNext(BulletItem);
        createdItem.SetData(ray.origin, ray.direction * ProjectileSpeed, new Action<Vector3>(SpawnImpactFromObjectPool));
        createdItem.Activate();
    }

    public void SpawnImpactFromObjectPool(Vector3 v)
    {
        var impact = ObjectPool.Current.GetNext(ImpactItem);
        impact.SetData(v);
        impact.Activate();



        var impactInstance = ObjectPool.Current.GetNext(FromThePool.ImpactExplosion);
        impact.SetData(v);
        impact.Activate();
    }
}
