﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Optimization;
using UnityEngine.Profiling;
using UnityEngine.UI;

public class DemoController : MonoBehaviour
{
    
    public Camera FocusCamera;
    public float ProjectileSpeed = 6;
    public Toggle UsePoolToggle = null;
    public Text FpsDisplay = null;
    public Slider ItemsPerClickSlider;
    public RectTransform UiPanel;
    public FromThePool BulletItem;
    public FromThePool ImpactItem;
    public Prefabs SourcePrefabs = new Prefabs();
    public float OffsetIncrement = 0.02f;


    bool UseResourcePool = true;
    Vector3[] excludeCorners = new Vector3[4];
    float currentOffsetAmount = 0;
    Recorder r;
    CustomSampler sampler;
    long? spawnPoolNanoseconds = null;
    long? spawnInstantiateNanoseconds = null;


    void Start ()
    {
        Application.targetFrameRate = 300;
        QualitySettings.vSyncCount = 0;
        UsePoolToggle.onValueChanged.AddListener(isChecked =>
        {
            UseResourcePool = isChecked;
        });
        UseResourcePool = UsePoolToggle.isOn;
        sampler = CustomSampler.Create("ObjectPool");
        r = sampler.GetRecorder();
    }

    void Update ()
    {

        if (UseResourcePool)
        {
            spawnPoolNanoseconds = r.elapsedNanoseconds;
        }
        else
        {
            spawnInstantiateNanoseconds = r.elapsedNanoseconds;
        }

        FpsDisplay.text = GetTimingText();

        if (MouseIsInExcludeZone(GetRect(UiPanel)) == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                r.enabled = true;
                sampler.Begin();
                currentOffsetAmount = 0;
                for (int i = 0; i < ItemsPerClickSlider.value; i++)
                {
                    currentOffsetAmount = i * OffsetIncrement;
                    SpawnBullet();
                }
                currentOffsetAmount = 0;
                sampler.End();
                r.enabled = false;
            }
        }
    }

    string GetTimingText()
    {
        if (spawnInstantiateNanoseconds.HasValue == false && spawnPoolNanoseconds.HasValue == false)
            return string.Empty;

        return string.Format("{0}\n{1}",
                        GetNanosecondString(spawnPoolNanoseconds, "Object Pool"),
                        GetNanosecondString(spawnInstantiateNanoseconds, "Regular Instantiation"));
    }

    bool MouseIsInExcludeZone(Rect r)
    {
        float mX = Input.mousePosition.x;
        float mY = Input.mousePosition.y;
        return
            mX >= r.x && mX <= r.x + r.width && mY >= r.y && mY <= Screen.height;
    }
    public Rect GetRect(RectTransform uiElement)
    {
        uiElement.GetWorldCorners(excludeCorners);
        var result = new Rect(
                      excludeCorners[0].x, excludeCorners[0].y,
                      excludeCorners[2].x - excludeCorners[0].x,
                      excludeCorners[2].y - excludeCorners[0].y);
        return result;
    }
    private string GetNanosecondString(long? v, string name)
    {
        if (v.HasValue == false || v.Value == 0)
            return string.Empty;

        return string.Format("{0:n} nanoseconds  ({1})", v.Value, name);
    }
    public void SpawnBullet()
    {
        var ray = FocusCamera.ScreenPointToRay(Input.mousePosition);
        ray.origin = ray.origin + (FocusCamera.transform.right * currentOffsetAmount);
        AbstractPooledItem createdItem = null;

        if (UseResourcePool)
        {
            createdItem = ObjectPool.Current.GetNext(BulletItem);
            createdItem.SetData(ray.origin, ray.direction * ProjectileSpeed, new Action<Vector3>(SpawnImpactFromObjectPool));
        }
        else
        {
            createdItem = Instantiate<AbstractPooledItem>(SourcePrefabs.Bullet);
            createdItem.SetData(ray.origin, ray.direction * ProjectileSpeed, new Action<Vector3>(SpawnImpactFromPrefab));
            createdItem.GetComponent<CollisionBroadcaster>().OnCollision = CollisionBroadcaster.BasicCollisionAction.Destory;
        }
        createdItem.Activate();
    }
    public void SpawnImpactFromPrefab(Vector3 v)
    {
        var impact = Instantiate<AbstractPooledItem>(SourcePrefabs.Impact);
        impact.SetData(v);
        impact.Activate();
        impact.GetComponent<AutoDisable>().ShouldDestroy = true;
    }
    public void SpawnImpactFromObjectPool(Vector3 v)
    {
        var impact = ObjectPool.Current.GetNext(ImpactItem);
        impact.SetData(v);
        impact.Activate();
    }

    [Serializable]
    public class Prefabs
    {
        public AbstractPooledItem Bullet;
        public AbstractPooledItem Impact;
    }
}
