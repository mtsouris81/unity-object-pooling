﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class SliderValueDisplay : MonoBehaviour {

    public string DisplayFormat = "{0}";
    Slider slider;
    Text text;

	void Start ()
    {

        text = GetComponent<Text>();
        slider = GetComponentInParent<Slider>();

        // attempt to find slider in self-node or child-node
        if (slider == null)
            slider = GetComponent<Slider>();

        if (slider == null)
            slider = GetComponentInChildren<Slider>(true);

        if (slider == null)
            throw new System.Exception("SliderValueDisplay failed to find Slider component");

        if (slider != null)
        {
            slider.onValueChanged.AddListener(SetTextValue);
            SetTextValue(slider.value);
        }
	}

    void SetTextValue(float val)
    {
        if (string.IsNullOrEmpty(DisplayFormat))
        {
            text.text = val.ToString();
        }
        else
        {
            text.text = val.ToString(DisplayFormat);
        }
    }
}
